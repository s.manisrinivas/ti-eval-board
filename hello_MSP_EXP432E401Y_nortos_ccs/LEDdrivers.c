/*
 * LEDdrivers.c
 *
 *  Created on: 1-Apr-2021
 *      Author: mani
 */

#include <stdint.h>
#include <stdbool.h>
#include "pinout.h"
#include "ti/devices/msp432e4/driverlib/inc/hw_gpio.h"
#include "ti/devices/msp432e4/driverlib/driverlib.h"
#include "LEDdrivers.h"


//*****************************************************************************
//
// Configure the GPIOLED and its pins.
//
//*****************************************************************************

void ConfigureLED(){
    //
    // Enable the GPIO Peripheral for LED port L and M ports.
    //
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOM);

    //
    // Enable the GPIO pins for the MASTER LED  (PL0) and CHANNEL LED'S  (PM0-7)
    //
    MAP_GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_0);
    MAP_GPIOPinTypeGPIOOutput(GPIO_PORTM_BASE, GPIO_PIN_0 |GPIO_PIN_1 |GPIO_PIN_2 |GPIO_PIN_3 |
                              GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);


}

void writeLED(int channelNumber, int state, int ledName){

    switch(channelNumber){

    case 1:
        if(ledName == LEDGREEN){

             if(state){
                 //IF state is on turn ON the LED CHANNEL-1
                GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_0, GPIO_PIN_0);
              }else{
               //IF state is on turn OFF the LED CHANNEL-1
              GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_0, 0);
           }
        }
        else if(ledName == LEDRED){

            if(state){
                  //IF state is on turn ON the LED CHANNEL-1
                 GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_1, GPIO_PIN_1);
               }else{
                //IF state is on turn OFF the LED CHANNEL-1
               GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_1, 0);
            }
        }

        break;
    case 2:
        if(ledName == LEDGREEN){

             if(state){
                 //IF state is on turn ON the LED CHANNEL-1
                GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_2, GPIO_PIN_2);
              }else{
               //IF state is on turn OFF the LED CHANNEL-1
              GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_2, 0);
           }
        }
        else if(ledName == LEDRED){

            if(state){
                  //IF state is on turn ON the LED CHANNEL-1
                 GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, GPIO_PIN_3);
               }else{
                //IF state is on turn OFF the LED CHANNEL-1
               GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_3, 0);
            }
        }

        break;
    case 3:
        if(ledName == LEDGREEN){

             if(state){
                 //IF state is on turn ON the LED CHANNEL-1
                GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_4, GPIO_PIN_4);
              }else{
               //IF state is on turn OFF the LED CHANNEL-1
              GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_4, 0);
           }
        }
        else if(ledName == LEDRED){

            if(state){
                  //IF state is on turn ON the LED CHANNEL-1
                 GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_5, GPIO_PIN_5);
               }else{
                //IF state is on turn OFF the LED CHANNEL-1
               GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_5, 0);
            }
        }

        break;
    case 4:
        if(ledName == LEDGREEN){

             if(state){
                 //IF state is on turn ON the LED CHANNEL-1
                GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_6, GPIO_PIN_6);
              }else{
               //IF state is on turn OFF the LED CHANNEL-1
              GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_6, 0);
           }
        }
        else if(ledName == LEDRED){

            if(state){
                  //IF state is on turn ON the LED CHANNEL-1
                 GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_7, GPIO_PIN_7);
               }else{
                //IF state is on turn OFF the LED CHANNEL-1
               GPIOPinWrite(GPIO_PORTM_BASE, GPIO_PIN_7, 0);
            }
        }

        break;

    }
}









