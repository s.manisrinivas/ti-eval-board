/*
 * RS485.h
 *
 *  Created on: 1-Apr-2021
 *      Author: mani
 */

#ifndef RS485_H_
#define RS485_H_


void configureRS485Channel();
void rs485ChannelWrite(int channel);
void rs485ChannelRead(int channel);
void powerSwitching();
void uartTransmit();
#endif /* RS485_H_ */
