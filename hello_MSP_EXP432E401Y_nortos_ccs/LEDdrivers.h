/*
 * LEDdrivers.h
 *
 *  Created on: 1-Apr-2021
 *      Author: mani
 */

#ifndef LEDDRIVERS_H_
#define LEDDRIVERS_H_

#define LEDON    1
#define LEDOFF   0

#define CHANNEL1   1
#define CHANNEL2   2
#define CHANNEL3   3
#define CHANNEL4   4

#define LEDRED     0
#define LEDGREEN   1

void ConfigureLED();
void writeLED(int baseaddress,int state , int ledName );

#endif /* LEDDRIVERS_H_ */
