/*
 * RS485.c
 *
 *  Created on: 1-Apr-2021
 *      Author: mani
 */

#include <stdint.h>
#include <stdbool.h>
#include "ti/devices/msp432e4/driverlib/driverlib.h"
#include "uartstdio.h"
#include "pinout.h"
#include "LEDdrivers.h"
#include "RS485.h"
uint32_t g_ui32SysClock;
void configureRS485Channel(){
    //
        // Enable the GPIO Peripheral used by the UART.
        //
        MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);


        MAP_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

        //
        // Initialize the UART for console I/O.
        //
        UARTStdioConfig(0, 115200, g_ui32SysClock);


        //enable clock for the UART
        SYSCTL->RCGCUART |= 0xFF;

        // Enable the GPIO Peripheral used by the UART.
        MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

        //Disable the alternate functions on this pins
         GPIOA->AFSEL= 0x00;

         //Baudrate - sys clock freq 25MHz
         //BR = freq /(16/115200)
         UART2->IBRD = 10;

         //uart line control register
         UART2->LCRH = 0x60;  /* 8-bit, no parity, 1-stop bit, no FIFO */
         //uart control register
         UART2->CTL = 0X301;

         // SYSTEM DELAY ......
         SysCtlDelay(g_ui32SysClock / 10 / 3);

}

void uartTransmit(){

    while((UART2->FR & 0x20) != 0){

    }
}

